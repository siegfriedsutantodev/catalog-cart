import { useEffect } from "react";
import Navbar from "./components/Navbar";
import CheckOut from "./components/CheckOut";
import CatalogWrapper from "./components/CatalogWrapper";
import ItemDetails from "./components/ItemDetails";
import { useSelector, useDispatch } from "react-redux";
import { total } from "./components/State/Slice/CartSlice";
import { Outlet, RouterProvider, createBrowserRouter } from "react-router-dom";
const Layout = () => {
  const { isOpen } = useSelector((state: any) => state.checkout);
  return (
    <div>
      <Navbar />
      {isOpen && <CheckOut />}
      <Outlet />
    </div>
  );
};
const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "/",
        element: <CatalogWrapper />,
      },
      {
        path: "/ItemDetails/:id",
        element: <ItemDetails />,
      },
    ],
  },
]);
const App = () => {
  const { cartItems } = useSelector((state: any) => state.cart);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(total());
  }, [cartItems]);
  return (
    <div className="font-RobotoRegular">
      <RouterProvider router={router}></RouterProvider>
    </div>
  );
};

export default App;