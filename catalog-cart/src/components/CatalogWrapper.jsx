import React from 'react'
import {items} from "../CartItems"
import CatalogItems from './CatalogItems'

const CatalogWrapper = () => {
  return (
    <div className="section grid lg:grid-cols-3 mid:grid-cols-2 gap-6">
        {items.map(item => {
            return <CatalogItems key={item.id} item={item} />
        })}
    </div>
  )
}

export default CatalogWrapper