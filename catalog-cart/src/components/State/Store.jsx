import { combineReducers, configureStore } from "@reduxjs/toolkit";
import checkOutReducer from "./Slice/CheckOutSlice";
import cartReducer from "./Slice/CartSlice";

const combinedReducer = combineReducers({
  checkout: checkOutReducer,
  cart: cartReducer,
});


export const store = configureStore({
  reducer: combinedReducer
})

// export type RootState = ReturnType<typeof combinedReducer>

