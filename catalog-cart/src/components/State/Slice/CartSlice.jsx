import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  cartItems: [],
  quantity: 0,
  total: 0,
};

const CartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addItem: (state, action) => {
      state.quantity++;
      const cartItem = state.cartItems.find(
        (cartItem) => cartItem.id === action.payload.id
      );
      cartItem
        ? (cartItem.quantity = cartItem.quantity + 1)
        : state.cartItems.push({ ...action.payload, quantity: 1 });
    },
    removeItem: (state, action) => {
      state.cartItems.map((cartItem) => {
        if (cartItem.id === action.payload.id) {
          state.cartItems = state.cartItems.filter(
            (item) => item.id !== cartItem.id
          );
          state.quantity = state.quantity - cartItem.quantity;
        }
      });
    },
    increaseQuantity: (state, action) => {
      state.quantity ++;
      const itemIndex = state.cartItems.findIndex(
        (cartItem) => cartItem.id === action.payload.id
      );
      state.cartItems[itemIndex].quantity ++;
      let total = state.cartItems[itemIndex].quantity * state.cartItems.price;
    },
    decreaseQuantity: (state, action) => {
      state.quantity --;
      const itemIndex = state.cartItems.findIndex(
        (cartItem) => cartItem.id === action.payload.id
      );
      if ( state.cartItems[itemIndex].quantity > 0 ) 
        state.cartItems[itemIndex].quantity--; 
    },
    total: (state) => {
      let total = 0;
      state.cartItems.forEach((cartItem) => {
        total += cartItem.quantity * cartItem.price;
      });
      state.total = total;
    },
    clearCart: (state) => {
      state.quantity = 0;
      state.cartItems = [];
    }
  },
});
export const { addItem, increaseQuantity, decreaseQuantity, removeItem, total, clearCart } =
  CartSlice.actions;
export default CartSlice.reducer;
