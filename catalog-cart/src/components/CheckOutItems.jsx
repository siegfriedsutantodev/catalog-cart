import React from "react";
import { HiX } from "react-icons/hi";
import { BsFillCartPlusFill, BsFillCartDashFill } from "react-icons/bs";
import { increaseQuantity, decreaseQuantity, removeItem } from "./State/Slice/CartSlice";
import { useDispatch } from "react-redux";
const CheckOutItems = ({ cartItem }) => {
  const dispatch = useDispatch();
  const { id, price, quantity, name, image } = cartItem;
  return (
    <div className="flex flex-col justify-between items-center border border-solid border-glass p-4 my-6">
      <div className="flex justify-between items-center w-full p-4">
        <div className="flex items-center gap-4">
          <img src={image} alt="" className="w-20 h-20 object-cover" />
        </div>
        <div className="flex flex-col items-start max-w-[6.8rem]">
          <div>{name}</div>
          <div className="flex items-center gap-4 my-2">
            <button
              className="w-8 h-8 text-white bg-black rounded-full flex justify-center items-center"
              onClick={() => dispatch(decreaseQuantity(cartItem))}
            >
              <BsFillCartDashFill className="cursor-pointer text-xl"/>
            </button>
            <div>{quantity}</div>
            <button
              className="w-8 h-8 text-white bg-black rounded-full flex justify-center items-center"
              onClick={() => dispatch(increaseQuantity(cartItem))}
            >
              <BsFillCartPlusFill className="cursor-pointer text-xl"/>
            </button>
          </div>
        </div>
        <div className="flex flex-col items-center gap-3">
          <HiX
            className="cursor-pointer text-xl"
            onClick={() => dispatch(removeItem(cartItem))}
          />
        </div>
      </div>
      <div className="flex justify-end items-center w-full p-4">
          <div>${(price * quantity).toFixed(2)}</div>
      </div>
    </div>
  );
};

export default CheckOutItems;
