import { defineConfig } from 'vite'
import {createHtmlPlugin} from 'vite-plugin-html'
import react from '@vitejs/plugin-react-swc'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    createHtmlPlugin({
      minify: true
    })
  ],
  optimizeDeps: {
    exclude: ['react-whatsapp']
  }
})
